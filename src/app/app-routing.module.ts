import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { StudentEnrollListComponent } from './course/student-enroll-list/student-enroll-list.component';
import { EnrolledListComponent } from './enrolled/enrolled-list.component';
import { DeleteListComponent } from './delete/delete-list.component';
import { GradeListComponent } from './grade/grade-list.component';
import { ResultGradingComponent } from './result-grading/result-grading.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/course/add',
    pathMatch: 'full'
  },
  { path: 'enrollActivity', component: StudentEnrollListComponent },
  { path: 'enrolledActivity', component: EnrolledListComponent },
  { path: 'deleteActivity', component: DeleteListComponent },
  { path: 'grade', component: GradeListComponent },
  { path: 'result', component: ResultGradingComponent },
  { path: '**', component: FileNotFoundComponent },


];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
