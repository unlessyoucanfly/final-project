import { Injectable } from '@angular/core';
import { LecturerService } from './lecturer.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Lecturer from '../entity/lecturer';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LecturerRestImplService extends LecturerService{
  constructor(private http: HttpClient) {
    super();
  }
  getLectures(): Observable<Lecturer[]>{
    return this.http.get<Lecturer[]>('assets/lecture.json');
  }

  getLecture(id:number): Observable<Lecturer>{
    return this.http.get<Lecturer[]>('assets/lecture.json')
    .pipe(map(lectures =>{
      const output: Lecturer = (lectures as Lecturer[]).find(lecture => lecture.id === +id);
      return output;
    }))
  }
}
