import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Observable } from 'rxjs';
import Student from '../entity/student';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import Course from '../entity/course';
import Enroll from '../entity/enroll';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService {

  public enroll:Enroll;
  constructor(private http: HttpClient) {
    super();
  }

  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('assets/student.json');
  }

  getStudent(id:number): Observable<Student>{
    return this.http.get<Student[]>('assets/student.json')
    .pipe(map(students =>{
      const output: Student = (students as Student[]).find(student => student.id === +id);
      return output;
    }))
  }

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>('assets/student.json', student);
  }

  enrollActivity(activity:Observable<Course>): Observable<Course> {
    throw this.http.post<Student>('assets/activity.json',activity);
  }

  enrolledActivity(id: number): Observable<Course[]> {
    return this.http.get<Course[]>('assets/activity.json');
  }

}
