import Lecturer from './lecturer';

export default class gradeCourse {
    id: number;
    activityId: string;
    activityName: string;
    semester: string;
    academic: string;
    lecturer:Lecturer;
    score:string;
  }